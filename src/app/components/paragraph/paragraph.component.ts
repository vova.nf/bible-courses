import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subscription} from "rxjs";
import {ParagraphModel} from "../../models/paragraph.model";
import {ParagraphService} from "../../services/paragraph.service";
import {ActivatedRoute, Router} from "@angular/router";
import {filter, map, switchMap} from "rxjs/operators";
import {ChapterService} from "../../services/chapter.service";
import {ChapterModel} from "../../models/chapter.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AnswerService} from "../../services/answer.service";
import {AnswerModel} from "../../models/answer.model";

@Component({
  selector: 'app-paragraph',
  templateUrl: './paragraph.component.html',
  styleUrls: ['./paragraph.component.scss']
})
export class ParagraphComponent implements OnInit, OnDestroy {
  chapter$: Observable<ChapterModel>;
  paragraph$: Observable<ParagraphModel>;

  next$: Observable<ParagraphModel>;
  prev$: Observable<ParagraphModel>;

  form: FormGroup;
  subscription = new Subscription();

  entities$ = this.answerService.entities$;

  constructor(
    private paragraphService: ParagraphService,
    private chapterService: ChapterService,
    private answerService: AnswerService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      answer: this.fb.control(null, [Validators.required]),
    });

    this.chapter$ = this.route.parent.params.pipe(
      switchMap((params) => this.chapterService.entities$.pipe(
        map((chapters: ChapterModel[]) => {
          return chapters.find((chapter: ChapterModel) => chapter.uuid === params.uuid)
        }),
        filter(result => !!result),
      )),
    );

    this.paragraph$ = this.route.params.pipe(
      switchMap((params) => this.chapter$.pipe(
        map(chapter => chapter.paragraphs.find((p: ParagraphModel) => p.uuid === params.uuid)),
      ))
    );

    this.subscription.add(
      this.paragraph$.pipe(
        switchMap(paragraph => this.answerService.entities$.pipe(
          map(entities => entities.find(entity => entity.paragraphUUid === paragraph.uuid)),
          filter(result => !!result),
        ))
      ).subscribe((result: AnswerModel) => {
        this.form.get('answer').setValue(result.answerUUid);
      })
    );

    this.prev$ = this.route.params.pipe(
      switchMap((params) => this.chapter$.pipe(
        map(chapter => {
          const idx = chapter.paragraphs.findIndex((p: ParagraphModel) => p.uuid === params.uuid);
          return chapter.paragraphs[idx - 1];
        })
      ))
    );

    this.next$ = this.route.params.pipe(
      switchMap((params) => this.chapter$.pipe(
        map(chapter => {
          const idx = chapter.paragraphs.findIndex((p: ParagraphModel) => p.uuid === params.uuid);
          return chapter.paragraphs[idx + 1];
        })
      ))
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  nextParagraph(current: ParagraphModel, next: ParagraphModel) {
    this.answerService.upsertOneInCache({
      paragraphUUid: current.uuid,
      answerUUid: this.form.get('answer').value,
    });
    this.router.navigate(['../', next.uuid], {relativeTo: this.route}).then();
  }

  goToResults(current: ParagraphModel) {
    this.answerService.upsertOneInCache({
      paragraphUUid: current.uuid,
      answerUUid: this.form.get('answer').value,
    });
    this.router.navigate(['../../', 'results'], {relativeTo: this.route}).then();
  }
}
