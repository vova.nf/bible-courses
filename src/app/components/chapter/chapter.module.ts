import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChapterRoutingModule } from './chapter-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ChapterRoutingModule
  ]
})
export class ChapterModule { }
