import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ParagraphComponent} from "../paragraph/paragraph.component";
import {SummaryComponent} from "../summary/summary.component";
import {ChapterComponent} from "./chapter.component";
import {ResultsComponent} from "../results/results.component";

const routes: Routes = [
  {
    path: ':uuid', component: ChapterComponent, children: [
      {
        path: 'paragraphs/:uuid', component: ParagraphComponent
      },
      {
        path: 'results', component: ResultsComponent,
      },
      {
        path: 'summary', component: SummaryComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChapterRoutingModule {
}
