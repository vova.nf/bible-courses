import {Component, OnInit} from '@angular/core';
import {filter, map, switchMap} from "rxjs/operators";
import {ChapterModel} from "../../models/chapter.model";
import {ActivatedRoute} from "@angular/router";
import {Observable} from "rxjs";
import {ChapterService} from "../../services/chapter.service";
import {AnswerService} from "../../services/answer.service";
import {AnswerModel} from "../../models/answer.model";

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})
export class ResultsComponent implements OnInit {
  chapter$: Observable<ChapterModel>;
  paragraphs$: Observable<any[]>;


  constructor(
    private route: ActivatedRoute,
    private chapterService: ChapterService,
    private answerService: AnswerService,
  ) {
  }

  ngOnInit(): void {
    this.chapter$ = this.route.parent.params.pipe(
      switchMap((params) => this.chapterService.entities$.pipe(
        map((chapters: ChapterModel[]) => {
          return chapters.find((chapter: ChapterModel) => chapter.uuid === params.uuid)
        }),
        filter(result => !!result),
      )),
    );

    this.paragraphs$ = this.route.params.pipe(
      switchMap(() => this.chapter$.pipe(
        switchMap((chapter: ChapterModel) => this.answerService.entities$.pipe(
          map((entities: AnswerModel[]) => chapter.paragraphs.map(paragraph => {
            return {
              ...paragraph,
              choices: paragraph.choices.map(choice => {
                const e = entities.find(entity => entity.paragraphUUid === paragraph.uuid);
                return {
                  ...choice,
                  selected: e ? e.answerUUid === choice.uuid : false
                }
              })
            }
          })),
        )))
      )
    )
  }


}
